const listaTarea = document.querySelector('.listaTarea');
document.querySelector('#formulario').addEventListener('submit', agregarTareaLocalStorage);
function agregarTareaLocalStorage(e){
	e.preventDefault();
	const nombre = document.querySelector('.nombre').value;
	const tarea = document.querySelector('.tarea').value;
	let listTareas = {
		nombre,
		tarea
	}
	let tareas;
	tareas = obtenerLocalStorage();
	tareas.push(listTareas);
	localStorage.setItem('tareas', JSON.stringify(tareas));
	
	document.querySelector('#formulario').reset();
	cargarLocalStorage();
	e.preventDefault();
}
function obtenerLocalStorage(){
	let tareas ;
	if(localStorage.getItem('tareas') === null){
		tareas = [];
	} else {
		tareas = JSON.parse(localStorage.getItem('tareas'));
	}
	return tareas;
}
function cargarLocalStorage(){
	let tareas = obtenerLocalStorage();
	listaTarea.innerHTML = '';
	tareas.forEach(element => {
		const li = document.createElement('li');
		li.innerHTML += `${element.nombre} - ${element.tarea} <a class="eliminar" onclick="eliminarLocalStorage('${element.nombre}')">X</a>`;
		listaTarea.appendChild(li);
	});
}
cargarLocalStorage();
function eliminarLocalStorage(nombre){
	let tareas = obtenerLocalStorage();
	tareas.forEach((element, index) => {
		if(element.nombre === nombre){
			tareas.splice(index, 1);
		}
	});
	localStorage.setItem('tareas', JSON.stringify(tareas));
	cargarLocalStorage();
}